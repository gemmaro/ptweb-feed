# frozen_string_literal: true

directory :public do
  file :index_atom, 'index.atom'
end

directory :tmp do
  file :index_html, 'index.html'
end
