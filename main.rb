# frozen_string_literal: true

require 'net/http'
require 'pathname'
require 'oga'
require 'date'
require 'rss'
require 'uri'
require 'pathtree'

# Extract metadata for generating feed from ptweb website.
class PxtoneWeb
  TOP_PAGE_URL = URI(File.read('pxtone_web_url.txt').chomp)

  Music = Struct.new(:href,
                     :title,
                     :composer,
                     :published,
                     keyword_init: true)

  # Extract necessary information from XML element.
  module ElementRefinements
    refine Oga::XML::Element do
      def extract
        extract_fields
        Music.new(href: @href,
                  title: @title,
                  composer: @composer,
                  published: @published)
      end

      def extract_fields
        @href = xpath('a[1]/@href').first.value
        @title = xpath('a[1]').first.text
        @composer = xpath('a[2]').first.then { _1.nil? ? '' : _1.text }
        @published = xpath('div/div[1]').first.text.then { DateTime.parse _1 }
      end
    end
  end

  # Add information to item.
  module ItemRefinements
    refine RSS::Maker::Atom::Feed::Items::Item do
      def add_entry_info(entry)
        self.link = (TOP_PAGE_URL + entry.href).to_s
        self.title = entry.title
        self.date = entry.published.to_time
        self.author = entry.composer
      end
    end
  end

  # Add information to RSS maker.
  module FeedRefinements
    using ItemRefinements

    refine RSS::Maker::Atom::Feed do
      def add_feed_metadata
        'https://gemmaro.gitlab.io/ptweb-feed/index.atom'.tap do |feed_uri|
          channel.about = feed_uri
          channel.link = feed_uri
        end

        channel.title = 'ptweb'
        channel.description = 'pxtone web feed (unofficial)'

        channel.author = 'ptweb-feed'
        channel.date = Time.now
      end

      def add_musics(musics)
        items.do_sort = true

        musics.each do |entry|
          items.new_item do |item|
            item.add_entry_info(entry)
          end
        end
      end
    end
  end

  # Generate feed from Array.
  module ArrayRefinements
    using FeedRefinements

    refine Array do
      def generate_feed
        RSS::Maker.make('atom') do |maker|
          maker.add_feed_metadata
          maker.add_musics(self)
        end
      end
    end
  end

  using ElementRefinements
  using ArrayRefinements

  def feed
    File.read(PATHS.tmp.index_html)
        .then { Oga.parse_html(_1) }
        .xpath('/html/body/div/div[2]/div/div[2]/ul/li/div')
        .map(&:extract)
        .generate_feed
  end
end

PATHS = Pathtree.read('path.rb')

PxtoneWeb.new.feed.tap { File.write(PATHS.public.index_atom, _1) }
