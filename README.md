# ptweb feed

Generate a [pxtone web](https://ptweb.me) feed.

Prerequisites:

* Ruby
* Bundler

Run:

```shell-session
bundle
bundle exec rake
```

And subscribe `public/index.atom`.
